@extends('layouts.admin')

@section('content')
<h1>Users</h1>

<table style="width:100%">
    {{-- Headers --}}
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Foto</th>
        <th>E-mail</th>
        <th>Role</th>
        <th>Activo</th>
        <th>Creado hace</th>
        <th>Actualizado hace</th>
    </tr>

    {{-- Elements --}}
    @foreach ($users as $user)
    <tr>
        <td>{{$user->id}}</td>
        <td><a href="{{route('users.edit',['user'=>$user])}}">{{$user->name}}</td>
        <td><img height="50px" src="{{$user->photo? $user->photo->file:'No hay foto'}}" alt=" "></td>
        <td>{{$user->email}}</td>
        <td>{{$user->role->name}}</td>
        <td>{{$user->is_active==1? 'Si':'No'}}</td>
        <td>{{$user->created_at->diffforHumans()}}</td>
        <td>{{$user->updated_at->diffforHumans()}}</td>
    </tr>
    @endforeach


    @endsection