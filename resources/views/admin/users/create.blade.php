@extends('layouts.admin')

@section('content')

<h1>Create User</h1>

<form action="{{action('AdminUsersController@store')}}" method="POST" enctype="multipart/form-data">
    @csrf

    {{-- USER NAMEs --}}
    <div class="form-group">
        <label for="name">Nombre</label>
        <br>
        <input type="text" name="name" id="name">
    </div>

    {{-- EMAIL --}}
    <div class="form-group">
        <label for="email">E-mail</label>
        <br>
        <input type="text" name="email" id="email">
    </div>

    {{-- ROLES --}}
    <div class="form-group">
        <label for="role">Role</label>
        <br>
        <select name="role_id" id="">
            @foreach ($roles as $role)
            <option value="{{$role->id}}">{{$role->name}}</option>
            @endforeach
        </select>
    </div>

    {{-- ESTADO DEL USUARIO --}}
    <div class="form-group">
        <label for="is_active">Activo</label>
        <input type="checkbox" name="is_active" value="1">
    </div>

    {{-- CONTRASEÑA --}}
    <div class="form-group">
        <label for="pass">Contraseña</label>
        <br>
        <input type="password" name="password" id="pass">
    </div>

    <div class="form-group">
        <input type="file" name="photo_id" id="photo_id" class="form-control">
    </div>
    
    
    {{-- SUBMIT --}}
        <div class="form-group">
            <button type="submit" class="btn btn-success">Upload</button>
        </div>
</form>

@include('includes.error_forms')

@endsection