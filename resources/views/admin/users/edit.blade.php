@extends('layouts.admin')

@section('content')

<h1>Edit User</h1>

<div class="col-sm-3">
    <img class="img-responsive img-rounded" src="{{$user->photo? $user->photo->file:''}}" alt="No hay Foto usuario">
</div>

<div class="col-sm-9">
    <form action="{{action('AdminUsersController@update',$user->id)}}" method="PATCH" enctype="multipart/form-data">
        @csrf

        {{-- USER NAMEs --}}
        <div class="form-group">
            <label for="name">Nombre</label>
            <br>
            <input type="text" name="name" id="name" value={{$user->name}}>
        </div>

        {{-- EMAIL --}}
        <div class="form-group">
            <label for="email">E-mail</label>
            <br>
            <input type="text" name="email" id="email" value={{$user->email}}>
        </div>

        {{-- ROLES --}}
        <div class="form-group">
            <label for="role">Role</label>
            <br>
            <select name="role_id" id="" value={{$user->role->id}}>
                <option value="{{$user->role->id}}">{{$user->role->name}}</option>
                @foreach ($roles as $role)
                @if ($role->id != $user->role->id)
                <option value="{{$role->id}}">{{$role->name}}</option>
                @endif
                @endforeach
            </select>
        </div>

        {{-- ESTADO DEL USUARIO --}}
        <div class="form-group">
            <label for="is_active">Activo</label>
            <input type="checkbox" name="is_active" @if ($user->is_active) checked @endif >

        </div>

        {{-- CONTRASEÑA --}}
        <div class="form-group">
            <label for="pass">Contraseña</label>
            <br>
            <input type="password" name="password" id="pass">
        </div>

        <div class="form-group">
            <input type="file" name="photo_id" id="photo_id" class="form-control">
        </div>


        {{-- SUBMIT --}}
        <div class="form-group">
            <button type="submit" class="btn btn-success">Upload</button>
        </div>
    </form>

    @include('includes.error_forms')

    @endsection
</div>